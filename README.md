# This is the  IPGP PSS Team public repository

Official [frontpage](http://www.ipgp.fr/fr/pss/planetologie-sciences-spatiales)


## HowTo

 - [Visit the lab](HowToVisitPSS.md) 

 

 ## Seminars

  - PSS seminars occur in 522 room (5th floor of [Lamarck-B building](HowToVisitPSS.md))

  - The [Amazing PSS Seminar](Amazing_PSS_Seminar.md)


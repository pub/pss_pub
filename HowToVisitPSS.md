# How To Visit PSS @ Lamarck


The Team is located at 6th and 7th floor at Lamarck-A building.

The address is  35 rue Hélène Brion, 75013 Paris

Once in the lobby, of the main entrance, take the elevators on the right up to the 6th floor.

Then take the corridor on the right and stop at the only single front door with IPGP's logo on it.

This is where we are.

![](lmk.jpeg)
